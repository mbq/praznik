# praznik

[![CRAN downloads](https://cranlogs.r-pkg.org/badges/praznik)](https://cran.rstudio.com/web/packages/praznik/index.html)
[![CRAN version](https://www.r-pkg.org/badges/version/praznik?color=green)](https://cran.r-project.org/package=praznik)
[![codecov](https://codecov.io/gl/mbq/praznik/branch/main/graph/badge.svg)](https://codecov.io/gl/mbq/praznik)
[![pipeline status](https://gitlab.com/mbq/praznik/badges/main/pipeline.svg)](https://gitlab.com/mbq/praznik/commits/main)

Praznik is an R package containing a collection of information theory-based tools for feature selection and scoring.
The library is described in

- [Miron B. Kursa (2021). Praznik: High performance information-based feature selection. SoftwareX, 16, 100819.](https://doi.org/10.1016/j.softx.2021.100819)

Among others, it provides efficient implementations of several information-based filter methods, including CMIM, DISR, JMI, JMIM and mRMR, as well as corresponding feature scorers allowing for experiments and development of novel methods.

You can install from [CRAN](https://cran.r-project.org/package=praznik), or directly from GitLab with:

```r
devtools::install_gitlab('mbq/praznik')
```
